import sys
import psycopg2

def execute(query, params=None):
    try:
        connection = psycopg2.connect("dbname='prints_2' host='localhost' " \
                                      "user='' password=''")
    except:
        print "Unable to connect the database."
        sys.exit('Exiting...')

    cursor = connection.cursor()
    connection.autocommit = True
    cursor.execute(query, params)
    return cursor.fetchall()

def fetch_fingerprint(identifier):
    fingerprints_with_motifs = []
    fingerprints = execute('SELECT id, identifier, accession, summary ' \
                           'FROM fingerprint ' \
                           'WHERE identifier = %s', (identifier,))
    for fingerprint in fingerprints:
        motifs = fetch_motifs(fingerprint[0])
        fingerprints_with_motifs.append([fingerprint, motifs])

    return fingerprints_with_motifs

def fetch_motifs(fingerprint_id):
    motifs_with_variants = []
    motifs = execute('SELECT motif_id, code, title ' \
                     'FROM motif ' \
                     "WHERE fingerprint_id = %s AND position = 'final'", (fingerprint_id,))
    for motif in motifs:
        sequences = fetch_sequences(motif[0])
        motifs_with_variants.append([motif, sequences])

    return motifs_with_variants

def fetch_sequences(motif_id):
    sequences = execute('SELECT sequence, pcode, start, interval ' \
                        'FROM seq ' \
                        'WHERE motif_id = %s', (motif_id,))
    return sequences
