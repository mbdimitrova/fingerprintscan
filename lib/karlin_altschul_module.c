/**************** Statistical Significance Parameter Subroutine ****************

        Version 1.0     February 2, 1990
        Version 2.0     March 18,   1993
        Version 3.0     April 8,    2004
        Version 4.0     March 13,   2013

        Program by:     Stephen Altschul

        Address:        National Center for Biotechnology Information
                        National Library of Medicine
                        National Institutes of Health
                        Bethesda, MD  20894

        Internet:       altschul@ncbi.nlm.nih.gov

See:    Karlin, S. & Altschul, S.F. (1990) "Methods for assessing the
        statistical significance of molecular sequence features by using
        general scoring schemes,"  Proc. Natl. Acad. Sci. USA 87:2264-2268.

        Altschul, S.F. (1991) "Amino acid substitution matrices from an
        information theoretic perspective," J. Mol. Biol. 219:555-565.

        Computes the parameters lambda and K and H for use in calculating the
        statistical significance of high-scoring segments or subalignments.

        The scoring scheme must be integer valued.  A positive score must be
        possible, but the expected (mean) score must be negative.

        A program that calls this routine must provide the value of the lowest
        possible score, the value of the greatest possible score, and a pointer
        to an array of probabilities for the occurence of all scores between
        these two extreme scores.  For example, if score -2 occurs with
        probability 0.7, score 0 occurs with probability 0.1, and score 3
        occurs with probability 0.2, then the subroutine must be called with
        low = -2, high = 3, and pr pointing to the array of values
        { 0.7, 0.0, 0.1, 0.0, 0.0, 0.2 }.  The calling program must also provide
        pointers to lambda and K; the subroutine will then calculate the values
        of these two parameters.  In this example, lambda=0.330 and K=0.154.

        The parameters lambda and K can be used as follows.  Suppose we are
        given a length N random sequence of independent letters.  Associated
        with each letter is a score, and the probabilities of the letters
        determine the probability for each score.  Let S be the aggregate score
        of the highest scoring contiguous segment of this sequence.  Then if N
        is sufficiently large (greater than 100), the following bound on the
        probability that S is greater than or equal to x applies:

                P( S >= x )   <=   1 - exp [ - KN exp ( - lambda * x ) ].

        In other words, the p-value for this segment can be written as
        1-exp[-KN*exp(-lambda*S)].

        This formula can be applied to pairwise sequence comparison by assigning
        scores to pairs of letters (e.g. amino acids), and by replacing N in the
        formula with N*M, where N and M are the lengths of the two sequences
        being compared.

        In addition, letting y = KN*exp(-lambda*S), the p-value for finding m
        distinct segments all with score >= S is given by:

                               2             m-1           -y
                1 - [ 1 + y + y /2! + ... + y   /(m-1)! ] e

        Notice that for m=1 this formula reduces to 1-exp(-y), which is the same
        as the previous formula.

*******************************************************************************/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <Python.h>

#define MAXIT 500       /* Maximum number of iterations used in calculating K */

int gcd(int a, int b)
{
        int c;

        if (b<0) b= -b;
        if (b>a) { c=a; a=b; b=c; }
        for (;b;b=c) { c=a%b; a=b; }
        return a;
}

double* _karlin(int low, int high, double *pr)
        /* Lowest score (must be negative)    */
        /* Highest score (must be positive)   */
        /* Probabilities for various scores   */
{
        int     i,j,range,lo,hi,first,last;
        double  y,slope,delta,sum,Sum,beta,temp,temp2;
        double  *p,*P,*ptrP,*ptr1,*ptr2;
        double lambda, H, K;
        double *coefficients = (double*)malloc(3*sizeof(double));
        memset(coefficients, 0.0, 3*sizeof(double));

        /* Check that scores and their associated probabilities are valid     */

        if (low>=0) {
                fprintf(stderr,"Lowest score must be negative.\n");
                return coefficients;
        }
        for (i=range=high-low;i> -low && !pr[i];--i);
        if (i<= -low) {
                fprintf(stderr,"A positive score must be possible.\n");
                return coefficients;
        }
        for (sum=i=0;i<=range;sum+=pr[i++]) if (pr[i]<0) {
                fprintf(stderr,"Negative probabilities not allowed.\n");
                return coefficients;
        }
        if (sum<0.99995 || sum>1.00005) fprintf(stderr,"Probabilities sum to %.4f.  Normalizing.\n",sum);
        p= (double *) calloc(range+1,sizeof(double));
        for (Sum=low,i=0;i<=range;++i) Sum+=i*(p[i]=pr[i]/sum);
        if (Sum>=0) {
                fprintf(stderr,"Invalid (non-negative) expected score:  %.3f\n",Sum);
                return coefficients;
        }

        /* Calculate the parameters lambda and H */

        lambda= -log(p[range])/high;
        do {
                beta=exp(lambda);
                temp=exp(lambda*(low-1));
                y=slope=0;
                ptr1=p;
                for (i=low;i<=high;++i) {
                        y+=temp2= *ptr1++ * (temp*=beta);
                        slope+= temp2*i;
                }
                lambda-=delta=(y-1)/slope;
        }
        while (delta / lambda > 1e-6);
        H= lambda*slope/log(2.0);     /* Relative entropy parameter H, in bits */

       /* Calculate the pamameter K */

        Sum=lo=hi=0;
        P= (double *) calloc(MAXIT*range+1,sizeof(double));
        for (*P=sum=j=1;j<=MAXIT && sum>0.0001;Sum+=sum/=j++) {
                first=last=range;
                for (ptrP=P+(hi+=high)-(lo+=low);ptrP>=P;*ptrP-- =sum) {
                        ptr1=ptrP-first;
                        ptr2=p+first;
                        for (sum=0,i=first;i<=last;++i) sum+= *ptr1-- * *ptr2++;
                        if (first) --first;
                        if (ptrP-P<=range) --last;
                }
                temp=exp(lambda*(lo-1));
                for (sum=0,i=lo;i;++i) sum+= *++ptrP * (temp*=beta);
                for (;i<=hi;++i) sum+= *++ptrP;
        }
        for (i=low;!p[i-low];++i);
        for (j= -i;i<high && j>1;) if (p[++i-low]) j=gcd(j,i);
        K = (j*exp(-2*Sum))/(slope*(1.0-exp(- lambda*j)));
        free(p);
        free(P);
        /* Parameters calculated successfully */

        coefficients[0] = lambda;
        coefficients[1] = H;
        coefficients[2] = K;
        return coefficients;
}


static PyObject*
calculate(PyObject* self, PyObject* args)
{
    int low, high;
    PyListObject *probabilities = (PyListObject*)PyList_New(0);

    if (!PyArg_ParseTuple(args, "iiO!", &low, &high, &PyList_Type, &probabilities))
        return NULL;

    int size = PyList_Size(probabilities);
    double *pr = (double*)malloc(size*sizeof(double));
    int i = 0;

    for (i = 0; i < size; i++) {
        pr[i] = PyFloat_AsDouble(PyList_GetItem(probabilities, (Py_ssize_t)i));
    }

    double *result = _karlin(low, high, pr);
    int length = 3;
    PyObject* coefficients = PyTuple_New(length);

    for(i = 0; i < length; i++)
    {
         PyTuple_SET_ITEM(coefficients, i, Py_BuildValue("f", result[i]));
    }

    return coefficients;
}

static PyMethodDef KarlinAltschulMethods[] =
{
     {"calculate", calculate, METH_VARARGS, "Karlin & Altschul"},
     {NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC
initkarlin_altschul(void)
{
    (void) Py_InitModule("karlin_altschul", KarlinAltschulMethods);
}
