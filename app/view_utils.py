class ViewUtils:
    def row_class(self, index, last_index, full_match):
        if last_index:
            klass = 'border-bottom '
        else:
            klass = ''

        if index == 1 & full_match:
            return klass + 'best-match'
        elif full_match:
            return klass + 'full-match'
        else:
            return klass + 'match'
