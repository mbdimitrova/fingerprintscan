class FingerprintScore:
    """ FingerprintScore represents the score of a given fingerprint and
    has a list of motif scores
    """
    def __init__(self, fingerprint, motifs_count):
        self.fingerprint = fingerprint
        self.motifs_count = motifs_count
        self.motif_scores = []

    def add_motif_score(self, motif_score):
        self.motif_scores.append(motif_score)

    def total_score(self):
        return sum([score.top_match().profile_score for score in self.motif_scores])

    def is_full_match(self):
        return len(self.motif_scores) == self.motifs_count

    def __eq__(self, other):
        return self.total_score() == other.total_score()

    def __gt__(self, other):
        return self.total_score() > other.total_score()

    def __lt__(self, other):
        return self.total_score() < other.total_score()
