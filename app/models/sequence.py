from app import db


class Seq(db.Model):
    """ Sequence represents a set of amino acids, used for the characterizing
    of a motif
    """
    seq_id = db.Column(db.Integer, primary_key=True, index=True, nullable=False)
    motif_id = db.Column(db.Integer, db.ForeignKey('motif.motif_id'))
    sequence = db.Column(db.String(30))
    pcode = db.Column(db.String(20))
    start = db.Column(db.Integer)
    interval = db.Column(db.SmallInteger)
