from collections import Counter
import random
import numpy

import lib.karlin_altschul as karlin_altschul

MATRIX_MULTIPLIER = 10


class PositionAnalysis:
    """ Position Analysis represents each position in the motif, lists the
    amino acids that occur at that position and the number of occurrences
    of each amino acid
    """
    def __init__(self, motif, threshold, substitution_matrix):
        self.motif = motif
        self.threshold = threshold
        self.substitution_matrix = substitution_matrix

        self.motif_size = motif.length()
        self.variants_count = len(motif.variants())

        self.frequency_table = self.compute_frequencies(motif.variants())
        self.profile = self.compute_profile()
        self.location = 0.0
        self.scale = 0.0
#        self.compute_karlin_altschul() #TODO

    def compute_frequencies(self, variants):
        """ Every item of the array corresponds to a position and has a
        histogram of the occurencies of amino acids on that position in
        the variants
        """
        return [self.histogram(variants, i) for i in range(0, self.motif_size)]

    def histogram(self, variants, index):
        """ Histogram of amino acids on given position """
        return Counter([variant[index] for variant in variants])

    def compute_profile(self):
        """ The calculation of the profile of a given amino acid on a given
        position uses the frequency table and substitution matrix
        """
        profile = []
        for position in range(0, self.motif_size):
            profile.append(Counter())
            for amino_acid in self.frequency_table[position]:
                profile[position][amino_acid] = self.score(
                    position, amino_acid)
        return profile

    def compute_karlin_altschul(self):
        values = numpy.hstack([position.values() for position in self.profile])
        count = len(values)
        values = Counter(values)
        score_probabilities = {}

        for score in values:
            score_probabilities[score] = float(values[score]) / count

        minimum = min(score_probabilities.keys())
        maximum = max(score_probabilities.keys())

        probabilities = []
        for i in range(minimum, maximum + 1):
            probabilities.append(score_probabilities.get(i, 0.0))

        params = karlin_altschul.calculate(minimum, maximum, probabilities)
        [self.scale, _, self.location] = params

    def score(self, position, amino_acid):
        """ The score for each coordinate is constructed by scoring each
        of the amino acids occuring in the given position against the mutation
        (substitution) matrix and multiplying it by the specific positional
        weight of that amino acid
        """
        score = 0
        # amino acids in the given position
        positional_amino_acids = self.frequency_table[position]

        for positional_amino_acid in positional_amino_acids:
            # probability of finding the current positional amino acid
            positional_count = positional_amino_acids[positional_amino_acid]
            probability = float(positional_count) / self.variants_count

            # index of the current amino acid "a" in the substitution matrix
            score += probability * self.substitution_matrix.weight(
                amino_acid, positional_amino_acid)
        return int(score * MATRIX_MULTIPLIER)
