import numpy
import operator

from app.models.match import Match


class MotifScore:
   """ MotifScore contains a list of matches that occur. Each match
   represents a fragment of a sequence that has matched the given
   motif and scored above the given threshold.
   """

   def __init__(self, motif):
       self.motif = motif
       self.matches = []

   def add_match(self, profile_score, id_score, p_value, position, lenght, sequence):
       match = Match(profile_score, position, lenght, sequence, p_value, id_score)
       self.matches.append(match)

   def top_match(self):
       return max(self.matches, key=operator.attrgetter('profile_score'))
