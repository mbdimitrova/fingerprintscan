from app import db

THRESHOLD = 15  # default threshold is 15%
DISTANCE = 10
P_VALUE_THRESHOLD = 0.1  # 1e-1


class Fingerprint(db.Model):
    id = db.Column(db.Integer, primary_key=True, index=True, nullable=False)
    identifier = db.Column(db.String(15), index=True, nullable=False)
    accession = db.Column(db.String(40))
    no_motifs = db.Column(db.Integer)
    creation_date = db.Column(db.Date)
    update_date = db.Column(db.Date)
    title = db.Column(db.String(100))
    annotation = db.Column(db.Text)
    cfi = db.Column(db.Text)
    summary = db.Column(db.Text)
    parent_id = db.Column(db.Text)
    motifs = db.relationship('Motif', backref='fingerprint', lazy='dynamic')
