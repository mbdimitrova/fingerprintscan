import math

from app.models.fingerprint import Fingerprint
from app.models.motif import Motif
from app.models.sequence import Seq
from app.models.substitution_matrix import SubstitutionMatrix
from app.models.position_analysis import PositionAnalysis
from app.models.match import Match
from app.models.motif_score import MotifScore
from app.models.fingerprint_score import FingerprintScore

class DigestSequence:
    """Scan PRINTS with a raw protein sequence"""

    def __init__(self, sequence, threshold, substitution_matrix_code):
        self.sequence = sequence
        self.threshold = int(threshold)
        self.substitution_matrix = SubstitutionMatrix(int(substitution_matrix_code))

    def top_scoring_fingerprints(self):
        fingerprint_scores = []

        for fingerprint in Fingerprint.query.join(Motif, Seq).filter(Motif.position == 'final').all():
            motifs = fingerprint.motifs.filter(Motif.position == 'final')
            fingerprint_score = FingerprintScore(fingerprint, motifs.count())

            for motif in motifs:
                position_analysis = PositionAnalysis(motif, self.threshold, self.substitution_matrix)
                motif_score = self.digest(position_analysis, motif)
                if len(motif_score.matches) > 0:
                    fingerprint_score.add_motif_score(motif_score)

            if fingerprint_score.total_score() > 0:
              fingerprint_scores.append(fingerprint_score)

        return self.format_results(sorted(fingerprint_scores, reverse=True))

    def digest(self, position_analysis, motif):
        """ Takes the given unknown amino acid sequence, breaks it into motif
        length sized fragments and scores each of them according to the
        frequency table and profile of the motif.
        Returns the best scoring fragment, its position and the string
        which represent it
        """
        motif_length = motif.length()
        padding = motif_length - 1
        motif_score = MotifScore(motif)

        # Choose motif sized fragments and score:
        # Number of fragments of size m from a sequence of size n,
        # which is padded at both ends is f = n - (m - 1).
        # Padding at both ends means that every character is visited
        # by every part of the motif (consistency).
        sequence = padding * '#' + self.sequence + padding * '#'

        # calculate the number of fragments needed to cover the whole sequence
        fragments_number = len(sequence) - (motif_length - 1)

        # loop through all fragments
        for fragment_index in range(0, fragments_number):
            # Return values are evaluated depending on the score of the fragment.
            # The highest scoring fragment is returned along with its
            # position in the amino acid sequence and its score.
            # fragment_index represents the start position in the sequence
            # for the fragment in question, i.e. the offset

            fragment_end = fragment_index + motif_length
            fragment = sequence[fragment_index:fragment_end]
            identity = self.score_fragment(position_analysis.frequency_table, fragment)
            profile_score = self.score_fragment(position_analysis.profile, fragment)

            # If the score for the individual fragment of the sequence is
            # greater than or equal to the designated threshold, report it
            if profile_score >= self.threshold:
                p_val = self.p_value(profile_score, len(sequence), position_analysis)

                if not p_val:
                    p_val = 1.0 / pow(10.0, 16)

                id_score = identity * 100.0 / (motif_length * position_analysis.variants_count)
                motif_score.add_match(profile_score, id_score, p_val,
                                      fragment_index - padding + 1,
                                      len(fragment), fragment)
        return motif_score

    def score_fragment(self, table, fragment):
        """ Given a fragment and a table (frequency or profile), calculate
        the fragments's score by summing the table values of the amino acid
        on each position in the fragment
        """
        return sum([table[i][fragment[i]] for i in range(0, len(fragment))])

    def p_value(self, score, sequence_length, position_analysis):
        try:
            return 1 - math.exp(-position_analysis.location * sequence_length * math.exp(-1 * position_analysis.scale * score))
        except OverflowError:
            return

    def format_results(self, results):
        return { 'fingerprints': [{'name': result.fingerprint.identifier, 'motifs': [motif_score for motif_score in result.motif_scores], 'is_full_match': result.is_full_match()} for result in results]}
