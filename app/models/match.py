class Match:
    """ Match represents information and scores for a fragment of a sequence
    that has matched a given motif
    """
    def __init__(self, profile_score=0, position=0, length=0, sequence='',
                 p_value=0, id_score=0):
        self.profile_score = profile_score
        self.position = position
        self.length = length
        self.sequence = sequence
        self.p_value = p_value
        self.id_score = id_score

    def __eq__(self, other):
        return self.profile_score == other.profile_score & \
            self.position == other.position & \
            self.length == other.length & \
            self.sequence == other.sequence & \
            self.p_value == other.p_value & \
            self.id_score == other.id_score

    def __gt__(self, other):
        return self.position > other.position

    def __lt__(self, other):
        return self.position < other.position
