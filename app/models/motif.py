from app import db


class Motif(db.Model):
    """
    Motif represents short segments of the structure of a protein
    """
    motif_id = db.Column(db.Integer, primary_key=True, index=True, nullable=False)
    fingerprint_id = db.Column(db.Integer, db.ForeignKey('fingerprint.id'))
    title = db.Column(db.String(100))
    code = db.Column(db.String(15), nullable=False)
    length = db.Column(db.Integer)
    position = db.Column(db.String(7), nullable=False)
    sequences = db.relationship('Seq', backref='motif', lazy='joined')

    def variants(self):
        return [variant.sequence for variant in self.sequences]

    def length(self):
        if self.variants().count > 0:
            return len(self.variants()[0])
        else:
            return 0
