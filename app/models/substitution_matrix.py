import yaml
import yamlordereddictloader

class SubstitutionMatrix:
    def __init__(self, code):
        self.matrix = self.create_matrix(code)

    def create_matrix(self, code=None):
        stream = file('blosum.yml', 'r')
        matrices = yaml.load(stream, Loader=yamlordereddictloader.Loader)

        if code == 80:
            return matrices['BLOSUM80']
        elif code == 45:
            return matrices['BLOSUM45']
        else:
            return matrices['BLOSUM62']

    def weight(self, amino_acid, substitution):
        position = self.matrix.keys().index(substitution)
        return self.matrix.get(amino_acid)[position]
