from flask import render_template, flash, request
from app import app
from app.models.fingerprint import Fingerprint
from app.models.motif import Motif
from app.models.sequence import Seq
from app.models.digest_sequence import DigestSequence
from app.forms import SubmissionForm
from app.view_utils import ViewUtils

@app.route('/', methods=['GET', 'POST'])
def index():
    form = SubmissionForm(request.form)

    if request.method == 'POST':
      sequence = request.form['sequence']
      substitution_matrix_code = request.form['substitution_matrix']
      threshold = request.form['threshold']

      if form.validate():
          results = DigestSequence(sequence, threshold, substitution_matrix_code).top_scoring_fingerprints()
          return render_template('results.html', title='FingerPRINTScan results page', form=form, results=results, vu=ViewUtils())
      else:
          flash(form.errors)

    return render_template('index.html', title='FingerPRINTScan submission page', form=form)
