from wtforms import Form, TextField, TextAreaField, validators, ValidationError, StringField, SubmitField

def validate_sequence(form, field):
    if not all(letter in 'ARNDBCEQZGHILKMFPSTWYV' for letter in field.data.upper()):
        raise ValidationError('Sequence should contain only standard members of the single letter amino acid code')

def validate_substitution_matrix(form, field):
    if not field.data in ['62', '45', '80']:
        raise ValidationError('Substitution matrix type is not correct')

class SubmissionForm(Form):
    sequence = TextAreaField(
        'Protein sequence',
        validators=[validators.required(), validate_sequence]
    )

    substitution_matrix = StringField(
        'Substitution matrix',
        default='62',
        validators=[validators.required(), validate_substitution_matrix]
    )

    threshold = TextField(
        'Profile score threshold',
        default='100'
    )
